var net = require('net');
var request = require('request');
var fs = require('fs');
var watch = require('node-watch');

var srv = net.createServer(function (sock) {

});

var username = null;
var directory = null;

process.argv.forEach(function (val, index, array) {
    if (index == 2) {
        username = val;
    }
    if (index == 3) {
        directory = val;
    }
});

// start client
srv.listen(0, function () {
    console.log(username + ' ' + directory + ' ' + srv.address().port);

    fs.readdir(directory, function (err, files) {
        for (var i = 0; i < files.length; i++) {
            sendToServer(files[i]);
        }
        if (files.length == 0) {
            console.log(('no files'));
        }
    });
});

watch(directory, {recursive: false}, function (evt, name) {
    if(evt == 'update') {
        var dir = directory.replace('/', '\\');
        name = name.replace(dir, '');
        sendToServer(name);
    }
});

function sendToServer(file) {

    console.log(file);
    console.log('Upload start!', file);
    request.post({
        url: 'http://localhost:3000/upload',
        form: {
            'username': username,
            'file' : file,
            'filepath': __dirname + '/' + directory + '/' + file
        }
    }, function optionalCallback(err, httpResponse, body) {
        if (err) {
            return console.error('upload failed:', err);
        }
        console.log('Upload successful!', file);
    });
}