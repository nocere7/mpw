var express = require('express');
var app = express();
var bodyParser = require('body-parser')
var mv = require('mv');
var fs = require('fs');

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));

var disks = [];

// get disks
fs.readdir('disk', function (err, dirs) {
    for (var i = 0; i < dirs.length; i++) {
        disks.push({
            path: '/disk/'+ dirs[i] + '/',
            busy: false
        });
    }
});

app.post('/upload', bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}), function (req, res) {
    uploadFile(req, res);
});

function uploadFile(req, res) {
    var disk = getFirstFreeDisk();
    if (disk.disk != null) {
        setTimeout(function () {
            var dest = __dirname + disk.disk.path + req.body.file;
            mv(req.body.filepath, dest, function (err) {
                disks[disk.index]['busy'] = false;
                if (err) {
                    console.log(err);
                    res.send(err);
                    return false;
                }
                console.log(dest);
                res.send('success upload: ' + req.body.filepath);
            });
        }, (Math.floor(Math.random() * 20) + 1) * 200); // 200 ms - 4000 ms -

    } else {
        setTimeout(function () {
            console.log('wait');
            uploadFile(req, res);
        }, 100);
    }
}

function getFirstFreeDisk() {
    var index = null;
    var disk = null;
    for (var i = 0; i < disks.length; i++) {
        if (!disks[i]['busy'] && disk == null) {
            index = i;
            disks[i]['busy'] = true;
            disk = disks[i];
        }
    }
    return {
        disk: disk,
        index: index
    };
}

app.listen(3000);